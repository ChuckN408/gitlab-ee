export default {
  props: {
    allVersions: {
      type: Array,
      required: true,
    },
  },
};
