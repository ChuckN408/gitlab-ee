import Vue from 'vue';
import VueResource from 'vue-resource';
import axios from './lib/utils/axios_utils';

Axios.use(axios_utils);


export default {
  createNewDraft(endpoint, data) {
    const postData = Object.assign({}, data, { draft_note: data.note });
    delete postData.note;

    return Axios.http.post(endpoint, postData, { emulateJSON: true });
  },
  deleteDraft(endpoint, draftId) {
    return Axios.http.delete(`${endpoint}/${draftId}`, { emulateJSON: true });
  },
  publishDraft(endpoint, draftId) {
    return Axios.http.post(endpoint, { id: draftId }, { emulateJSON: true });
  },
  addDraftToDiscussion(endpoint, data) {
    return Axios.http.post(endpoint, data, { emulateJSON: true });
  },
  fetchDrafts(endpoint) {
    return Axios.http.get(endpoint);
  },
  publish(endpoint) {
    return Axios.http.post(endpoint);
  },
  discard(endpoint) {
    return Axios.http.delete(endpoint);
  },
  update(endpoint, { draftId, note, resolveDiscussion }) {
    return Axios.http.put(
      `${endpoint}/${draftId}`,
      { draft_note: { note, resolve_discussion: resolveDiscussion } },
      { emulateJSON: true },
    );
  },
};